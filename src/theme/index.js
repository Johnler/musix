import {useColorScheme} from 'react-native'


export const DefaultTheme = {
  light: {
    dark: false,
    colors: {
      primary: '#CE6FD5',
      secondary: '#76D56F',
      background: '#3C4040',
      backgroundSecondary: '#5e6464',
      card: '#FEFEFE',
      text: '#FEFEFE',
      border: '#FEFEFE',
    },
    font: {
      fontFamily: 'cream'
    }
  },
  dark: {
    dark: true,
    colors: {
      primary: '#CE6FD5',
      secondary: '#76D56F',
      background: '#3C4040',
      backgroundSecondary: '#5e6464',
      card: '#FEFEFE',
      text: '#FEFEFE',
      border: '#FEFEFE',
    },
    font: {
      fontFamily: 'cream'
    }
  }
} 



export const useTheme = () => {
  const isDarkMode = useColorScheme() === 'dark'
  if (isDarkMode){
    return  {theme: DefaultTheme.dark, colors: DefaultTheme.dark.colors, font: DefaultTheme.dark.font}
  } else return {theme: DefaultTheme.light, colors: DefaultTheme.light.colors, font: DefaultTheme.light.font}
}

