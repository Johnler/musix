import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useTheme } from '../theme';

import Home from "../screens/Home";
import Playlist from "../screens/Playlist";
import Settings from "../screens/Settings";

const mainScreens = {
    Home: {
        screen: Home
    },
    Playlist: {
        screen: Playlist
    },
    Settings: {
        screen: Settings
    }
}

const Tab = createBottomTabNavigator()


function MainScreens() {
    const {theme, colors} = useTheme()
    return(
        <NavigationContainer theme={theme}>
            <Tab.Navigator
            screenOptions={({route}) => ({
                headerShown: false,
                tabBarStyle: {backgroundColor: colors.background, borderTopColor: colors.primary},
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    if(route.name === 'Home') {
                        iconName = 'music'
                    }
                    else if (route.name === 'Playlist'){
                        iconName = 'list-ul'
                    }
                    else if (route.name === 'Settings'){
                        iconName = 'tools'
                    }
                    return <FontAwesome5 name={iconName} size={size} color={color}/>
                }
            })}
            >
                <Tab.Screen name="Home" component={Home}/>
                <Tab.Screen name="Playlist" component={Playlist}/>
                <Tab.Screen name="Settings" component={Settings}/>
            </Tab.Navigator>
        </NavigationContainer>
    )
}


export default MainScreens