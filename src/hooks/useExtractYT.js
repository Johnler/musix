import React, {useState, useEffect} from 'react'
import ytdl from 'react-native-ytdl'



function useExtractTY(url) {
   
    const [response, setResponse] = useState("")

    useEffect(() => {
        ytdownload()
    }, [url])

    const ytdownload = async() => {
        const info = await ytdl.getInfo(url, {quality: 'highestaudio'})
        const audioFormats = ytdl.filterFormats(info.formats, 'audioonly')
        setResponse(audioFormats[0].url)
      }
      
      return response

}


export default useExtractTY;
