import React from 'react'
import { View, Pressable } from 'react-native'
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { useTheme } from '../../theme'

function FAB() {
    const { colors } = useTheme()
    return(

        <Pressable
        style={{
            borderWidth: 1,
            borderColor: colors.background,
            alignItems: 'center',
            justifyContent: 'center',
            width: 70,
            position: 'absolute',
            bottom: 30,
            right: 10,
            height: 70,
            backgroundColor: colors.backgroundSecondary,
            borderRadius: 100,
        }}
         >
             <FontAwesomeIcon name="plus" color={colors.secondary} size={20}/>
        </Pressable>
    )
}

export default FAB