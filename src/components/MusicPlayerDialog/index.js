import React, {useState, useEffect} from "react";
import {View, Pressable} from 'react-native';
import Text from '../../components/Text'
import TrackPlayer, {   Capability,
    Event,
    RepeatMode,
    State,
    usePlaybackState,
    useProgress,
    useTrackPlayerEvents, } from 'react-native-track-player'
import ModalDialog from "../ModalDIalog";
import {Bar} from 'react-native-progress'
import {useTheme} from '../../theme'
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import logger from '../../utils/logger'
import useExtractTY from "../../hooks/useExtractYT";




function MusicPlayer({progress, duration, progressColor, playing, playControl}) {
    const convert = ((progress * 0.1) / (0.0100*duration) * 0.1)
    const splitB = progress ? convert.toString().split('.')[1]: 0
    const prog = +`.${splitB}`
    return(
        <View style={{
            alignItems: 'center',
        }}>
            <Bar style={{marginTop: 10, marginBottom: 10}} progress={prog} width={270} color={progressColor} />
            <Pressable onPress={playControl}>
                {
                    playing ? <FontAwesome5Icon name="pause" size={30}/>
                            : <FontAwesome5Icon name="play" size={30}/>
                }
            </Pressable>
        </View>
    )
}

function MusicPlayerDialog({visible, onPressNegative, ytinfo}) {
    const {
        title,
        artist,
        url,
        id
        } = ytinfo
    const {colors} = useTheme()
    const progress = useProgress()
    const [playing, setPlaying] = useState(false)
    const playbackState = usePlaybackState();
    
    
    const parseYTURL = useExtractTY(url)

    
        useEffect(()=> {
            parseYTURL && setupPlayer()
        }, [parseYTURL])


    const setupPlayer = async () => {
        await TrackPlayer.setupPlayer();
        await TrackPlayer.updateOptions({
            stopWithApp: false,
            capabilities: [
              Capability.Play,
              Capability.Pause,
            //   Capability.SkipToNext,
            //   Capability.SkipToPrevious,
              Capability.Stop,
            ],
            compactCapabilities: [Capability.Play, Capability.Pause],
          });
        await TrackPlayer.add({
            id: id,
            url: parseYTURL,
            title: title,
            artist: artist,
        })
    
    }

    const togglePlayback = async (playbackState) => {
        const currentTrack = await TrackPlayer.getCurrentTrack();
        if (currentTrack == null) {
          // TODO: Perhaps present an error or restart the playlist?
        } else {
          if (playbackState !== State.Playing) {
            await TrackPlayer.play();
            setPlaying(true)
          } else {
            await TrackPlayer.pause();
            setPlaying(false)
          }
        }
      };


    const destroyPlayer = () => {
        setPlaying(false)
        TrackPlayer.pause()
        TrackPlayer.destroy()
        onPressNegative()
    }
    
    return (
        <ModalDialog
            visible={visible}
            closeIcon={true}
            body={
                <View>
                    <Text>{title}</Text>
                    <MusicPlayer 
                    progressColor={colors.secondary}
                    progress={progress.position}
                    duration={progress.duration}
                    // playControl={() => setPlaying(!playing)}
                    playControl={() => togglePlayback(playbackState)}
                    playing={playing}
                    />
            </View>
        }
            onPressNegative={destroyPlayer}
        />
    )
}


export default React.memo(MusicPlayerDialog)