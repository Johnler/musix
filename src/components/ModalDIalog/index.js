import React from "react";
import { View, Modal, StyleSheet, Pressable, Button } from 'react-native';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { useTheme } from "../../theme";

const ModalDialog = ({
      visible, 
      header, 
      body,
      closeIcon = false,
      negativeButtonTitle,
      positiveButtonTitle,
      onPressNegative, 
      onPressPositive
}) => {
      const {colors} = useTheme()
     return(
           <Modal
           visible={visible}
           transparent={true}
           animationType="fade"
           >
            <View style={style.centeredView}>
                  <View style={{...style.modalView, backgroundColor: colors.backgroundSecondary}}>
                        <View style={style.modalClose}>
                              {closeIcon && <Pressable onPress={onPressNegative}>
                                    <FontAwesomeIcon name="times-circle" size={20} />
                              </Pressable>}
                        </View>
                        <View style={style.modalHeader}>
                              {header}
                        </View>
                        <View style={style.modalBody}>
                              {body}
                        </View>
                        <View style={style.modalFooter}>
                              <View style={{ flex: 1, flexDirection: 'row'}}>
                                    <View style={{flex: 1, margin: 2}}>
                                          {negativeButtonTitle && <Button title={negativeButtonTitle} color={colors.background} onPress={onPressNegative}  />}
                                    </View>
                                    <View style={{flex: 1,  margin: 2}}>
                                          {positiveButtonTitle && <Button title={positiveButtonTitle} color={colors.primary} onPress={onPressPositive} />}
                                    </View>
                              </View>
                        </View>
                 </View>
            </View>
           </Modal>
     )
}


const style = StyleSheet.create({
      centeredView: {
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 22
      },
      modalClose: {
            alignSelf: 'flex-end'
      },
      modalView: {
            margin: 20,
            borderRadius: 10,
            padding: 10,
            shadowColor: "#000",
            shadowOffset: {
                  width: 0,
                  height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5,
            width: '80%',
      },
      modalHeader: {

      },
      modalBody: {
            marginTop: 20,
            marginBottom: 20
      },
      modalFooter: {
            flexDirection: 'row'
      }
})

export default ModalDialog