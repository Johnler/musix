import React from 'react'
import {Text, StyleSheet} from 'react-native'
import { useTheme } from '../../theme'


function TextComponent(props) {
    const {style, ...rest} = props
    const {font} = useTheme()

    return(
        <Text style={{...style, ...font}} {...rest}>{props.children}</Text>
    )
}

export default TextComponent
