import React from 'react'
import { View, TextInput, StyleSheet, Pressable, Keyboard } from 'react-native'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import {useTheme} from '../../theme'


function TextInputSearch({value, onChangeText, onPress}) {
    const {colors} = useTheme()

    const handleOnPress = () => {
        onPress()
        Keyboard.dismiss()
    }
    return(
        <View style={{...styles.body, borderColor: colors.border}}>
            <TextInput autoCapitalize='none' value={value} onChangeText={onChangeText} style={styles.textInputSearch} />
            <Pressable onPress={handleOnPress} style={pressStyle}>
                <FontAwesome5Icon  name='search' size={25} color={colors.primary} />
            </Pressable>
        </View>
    )
}


const styles = StyleSheet.create({
    body: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 30,
    },
    textInputSearch: {
        fontSize: 20,
        flex: 1,
        padding: 10
    },
    iconPressable: {
        margin: 5,
        marginRight: 10,
        borderRadius: 50,
        padding:8
    }
})

const pressStyle = ({pressed}) => {
    const {colors} = useTheme()
    return [
        {
            backgroundColor: pressed ? colors.secondary : colors.background
        }, 
        styles.iconPressable
    ]
}

export default TextInputSearch
