import { createSlice } from "@reduxjs/toolkit";

export const musicPlaylistSlice = createSlice({
  name: "musicplaylist",
  initialState: {
    musiclist: [
        {
            title: 'Yow',
            thumbnail: '',
            url: 'youtube.com/buangka'
        }
    ]
  },
  reducers: {
    addMusic: (state, action) => {
      state.musiclist = [...state.musiclist, action.payload]
    }
  }
})


export const { addMusic } = musicPlaylistSlice.actions

export default musicPlaylistSlice.reducer