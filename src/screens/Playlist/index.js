import React from 'react'
import { View, Pressable } from 'react-native'
import {Text, FAB} from '../../components/'
import MusicSectionPlayList from './components/MusicSectionPlayList'
import { useSelector, useDispatch } from 'react-redux'
import logger from '../../utils/logger'

function Playlist() {
    const musicPlaylistState = useSelector((state) => state.musiclist)
    logger(musicPlaylistState)
    return(
    <View style={{
        flex: 1,
    
    }}>

        <Text>Playlist</Text>
        <MusicSectionPlayList />
        <FAB />
        </View>
    )
}

export default React.memo(Playlist)