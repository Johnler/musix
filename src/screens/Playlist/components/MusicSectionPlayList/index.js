import React from 'react'
import { SectionList, View, StyleSheet } from 'react-native'
import Text from '../../../../components/Text'
import { useTheme } from '../../../../theme'


function MusicSectionItem({title}) {
    return (
        <View>
            <Text>{title}</Text>
        </View>
    )
}


function MusicSectionPlayList() {
    const { colors } = useTheme()


    const DATA = [
        {title: 'Section 1', data: ["a","b","c"]},
        {title: 'Section 2', data: ["a","b","c"]},
        {title: 'Section 3', data: ["a","b","c"]},
        {title: 'Section 3', data: ["a","b","c"]},
        {title: 'Section 3', data: ["a","b","c"]},
        {title: 'Section 3', data: ["a","b","c"]},
        {title: 'Section 3', data: ["a","b","c"]},
        {title: 'Section 3', data: ["a","b","c"]},

    ]
    

    return(
        <SectionList 
            sections={DATA}
            keyExtractor={(item, index) => item + index}
            renderItem={({item}) => <MusicSectionItem title={item}/>}
            renderSectionHeader={
                ({section: {title}}) => (
                <View style={{...styles.sectionHeader, backgroundColor: colors.primary}}>
                    <Text style={{...styles.sectionHeaderText}}>{title}</Text>
                </View>
                )
            }
        />
    )
}


const styles = StyleSheet.create({
    sectionHeader: {
        height: 50,
        justifyContent: 'center'
    },
    sectionHeaderText: {
        fontSize: 25,
        fontFamily: 'cream'
    }
})


export default MusicSectionPlayList
