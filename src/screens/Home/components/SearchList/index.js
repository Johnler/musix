import React, {useEffect} from 'react'
import {
    View,  
    FlatList, 
    Image, 
    StyleSheet,
    Alert,
    Pressable
} from 'react-native'
import Text from '../../../../components/Text'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'

import {numberSuffix} from '../../../../utils/numberFormatter'
import { useTheme } from '../../../../theme'

const Item = (items) => {
    const {colors} = useTheme()
    const { item, setYTI } = items
    const { 
        author,
        badges,
        bestThumbnail,
        description,
        duration,
        id,
        isLive,
        isUpcoming,
        thumbnails,
        title,
        type,
        upcoming,
        uploadedAt,
        url,
        views,
     } = item

    const handlePlayYT = () => {
        setYTI({
            url: url,
            id: id,
            artist: author.name,
            title: title
        })
    }

    return (
     type === "video" ?
    <View 
        borderRadius={5}
        borderColor={colors.border}
        padding={1}
        margin={5}
        style={{...styles.itemBox, backgroundColor:colors.background}}
    >
        <View style={styles.itemBoxContent}>
            { bestThumbnail && 
                <Pressable onPress={handlePlayYT}>
                    <Image style={styles.thumbnail} source={{uri:bestThumbnail.url }} /> 
                </Pressable>
            }
            {/* { bestThumbnail && (<View style={styles.thumbnail}><ImagePlayer onPress={() => handlePlayYT()} id={id} imgSource={bestThumbnail.url} artist={author.name} title={title} /></View>)} */}
            
            <View style={styles.itemBoxContentInfo}>
                <Text style={styles.itemBoxTitle}>{title}</Text>
                <View style={styles.itemBoxViews}>
                    <FontAwesome5Icon name="eye" style={{marginRight: 3}}/>
                    <Text style={styles.itemBoxAuthorName}>{views && numberSuffix(views)}</Text>
                </View>
                <View style={styles.itemBoxViews}>
                    <FontAwesome5Icon name="user-circle" style={{marginRight: 3}}/>
                    <Text style={styles.itemBoxAuthorName}>{author.name}</Text>
                </View>
            </View>
            {/* <IconButton onPress={() => handleDownloadButton()} icon={<Icon as={<FontAwesomeIcon icon={faDownload} size={20} color="black" />}/>} />  */}
        </View>
    </View>
    : <></>)  
}


function ItemList(props) {
    const { searchResult, setYTI } = props
    return (
        <FlatList
            data={searchResult.items}
            renderItem={(items) => <Item {...items} setYTI={setYTI} />}
            keyExtractor={(items) => items.id}
            style={{height: '90%'}}
        />
    )
}

const styles = StyleSheet.create({
    thumbnail: {
        width: 100,
        height: 70
    },
    itemBox: {
        flex: 1,
    },
    itemBoxContent: {
        flexDirection: 'row',
    },
    itemBoxContentInfo: {
        flex: 1,
        padding: 2
    },
    itemBoxTitle: {
        flex: 1,
        flexWrap: 'wrap',
        fontWeight: '500'
    },
    itemBoxAuthorName: {
        fontSize: 10,
        fontStyle: 'italic'
    },
    itemBoxViews: {
        flex: 1, 
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export default React.memo(ItemList)