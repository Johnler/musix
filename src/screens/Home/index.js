import React, { useEffect, useState } from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { PermissionsAndroid } from 'react-native'
import ytsr from 'react-native-ytsr'
import TextInputSearch from '../../components/TextInputSearch'
import SearchList from './components/SearchList'
import MusicPlayerDialog from '../../components/MusicPlayerDialog'
import logger from '../../utils/logger'


function Home() {
    const [inputSearch, setInputSearch] = useState('')
    const [ytsState, setytsState] = useState({})
    const [showMusicPlayerModal, setShowMusicPlayerModal] = useState(false)
    const [ytinfo, setYtinfo] = useState({
        url: '',
        id: '',
        artist: '',
        title: '',
      })

      useEffect(() => {
        requestPermissions()
      }, [])
    
      useEffect(() => {
        ytinfo.url && setShowMusicPlayerModal(!showMusicPlayerModal)
      }, [ytinfo])

    const requestPermissions = async () => {
        try {
          const write = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
              title: 'Storage',
              message: 'Musix needs access to your Files',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK'
            }
          )
          const read = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
              title: 'Storage',
              message: 'Musix needs access to your Files',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK'
            }
          )
          // if (write === PermissionsAndroid.RESULTS.GRANTED && read === PermissionsAndroid.RESULTS.GRANTED) 
        } catch (err) {
          logger("Permission denied")
        }
      }

      const ytsearch = async(keyword) => {
        const filter = await ytsr.getFilters(keyword)
        const filterTypeVideo = filter.get('Type').get('Video')
        const result = await ytsr(filterTypeVideo.url)
        setytsState(result)
      }
    
      const handleSearchButton = () => {
        ytsearch(inputSearch)
      }

      const handleSetYTI = (payload) => {
        setYtinfo(payload)
      }

    return(
        <SafeAreaView>
            <View style={styles.body}>
                <TextInputSearch onPress={handleSearchButton} onChangeText={setInputSearch} value={inputSearch}/>
            </View>
            <SearchList searchResult={ytsState} setYTI={setYtinfo} />
          <MusicPlayerDialog 
            visible={showMusicPlayerModal}
            onPressNegative={() => { setShowMusicPlayerModal(!showMusicPlayerModal)}}
            ytinfo={ytinfo}
          />

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    body: {
        padding: 5
    }
})

export default React.memo(Home)