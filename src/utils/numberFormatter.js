


function numberSuffix(number){
    const num = number.toString()
    if(num.length === 4) return `${num[0]}K`
    else if(num.length === 5) return `${num[0]}${num[1]}K`
    else if(num.length === 6) return `${num[0]}${num[1]}${num[2]}K`
    else if(num.length === 7) return `${num[0]}M`
    else if(num.length === 8) return `${num[0]}${num[1]}M`
    else if(num.length === 9) return `${num[0]}${num[1]}${num[2]}M`
    else if(num.length === 10) return `${num[0]}B`
}


export { numberSuffix }