import {Platform} from 'react-native';

export default (...args) =>
  console.log(
    `%c [${Platform.OS}]: `,
    'font-size:20px;background-color: #42b983;color:#fff;',
    ...args,
  );
