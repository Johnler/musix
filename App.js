
import React from 'react';
import Navigation from './src/navigations/MainNavigation'
import { store, persistor } from './src/redux'
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';

const App = () => {

  return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
        <Navigation />
        </PersistGate>
      </Provider>
  );
};


export default App;
